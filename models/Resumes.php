<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for table "resumes".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $keywords
 * @property string $resumes
 */
class Resumes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $upload;
    public static function tableName()
    {
        return 'resumes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'keywords'], 'required'],
            [['keywords'], 'string'],
            [['firstname', 'lastname', 'resumes'], 'string', 'max' => 255],
            [['upload'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf, doc, docx, txt'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'keywords' => 'Keywords',
            'resumes' => 'Resumes',
        ];
    }

}
