$('#search').on('click', function () {
    if ($('#first-name').val().length == 0 && $('#last-name').val().length == 0 && $('#keywords').val().length == 0 && $('#resume').val().length == 0) {
        alert("Fill at least one field");
    } else {
        var arr = {
            firstname: $('#first-name').val(),
            lastname: $('#last-name').val(),
            keywords: $('#keywords').val(),
            resumes: $('#resume').val()
        };
        // arr.push($('#first-name').val(), $('#last-name').val(), $('#keywords').val(), $('#resume').val());
        var data = [];
        // data = arr.filter(function (item,key) {
        //     return item.key != "";
        // });
        $.ajax({
            method: "POST",
            url: "resumes/search",
            data: arr,
            success: function (resultData) {
                if (!resultData) {
                    alert("Nothing found on your request")
                } else {
                    resultData = JSON.parse(resultData);
                    $.each(resultData,function (key,value) {
                        var resultHTML =    '<div class = "row">' +
                            ' <div class = "form-group search-result"> ' +
                            '<div class = "col-lg-12">' +
                            ' <div class="col-md-3">'+value.firstname+'</div>' +
                            ' <div class="col-md-3">'+value.lastname+'</div>' +
                            ' <div class="col-md-3">'+value.keywords+'</div> ' +
                            '<div class="col-md-3"><a href = "'+value.resumes+'" download = "'+value.resumes.substring(8)+'">'+value.resumes.substring(8)+'</div>' +
                            ' </div>' +
                            ' </div>' +
                            ' </div>';
                        $('.body-content').append(resultHTML);
                    });


                    console.log(resultData);


                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("some error");
            }
        });
    }
});
