<?php

use yii\db\Migration;

class m170920_202132_resumes extends Migration
{
    public function safeUp()
    {
        $this->createTable('resumes', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull(),
            'lastname' => $this->string()->notNull(),
            'keywords' => $this->text(),
            'resumes' => $this->string()->notNull(),
        ]);
    }

    public function safeDown()
    {
        echo "m170920_202132_resumes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170920_202132_resumes cannot be reverted.\n";

        return false;
    }
    */
}
