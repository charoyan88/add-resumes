<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Resumes';
?>
<div class="site-index">



    <div class="body-content">
        <div class="row">
            <div class="text-right">
                <a href="<?=Url::to('/resumes/create');?>" class="btn btn-success">Add Resume</a>
            </div>
            <div class="form-group search-form">
                <div class="col-lg-12">
                    <div class="col-md-2">
                        <label for="first-name">First Name</label>
                        <input type="text" id="first-name" class="form-control" name="first_name" placeholder="First Name"/>
                    </div>
                    <div class="col-md-2">
                        <label for="last-name">Last Name</label>
                        <input type="text" id="last-name" class="form-control" name="last_name" placeholder="Last Name"/>
                    </div>
                    <div class="col-md-4">
                        <label for="keywords">Keywords</label>
                        <input type="text" id="keywords" class="form-control" name="keywords" placeholder="Keywords"/>
                    </div>
                    <div class="col-md-3">
                        <label for="resume">Resume</label>
                        <input type="text" id="resume" class="form-control" name="resume" placeholder="Resume"/>
                    </div>
                    <div class="col-md-1 search-button-section">
                        <button type="button" id="search" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
