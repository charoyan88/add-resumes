<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

//$this->title = 'About';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <h2> Task description </h2>
    <p>
        The page must give user opportunity to add items(people profiles) and then search among them.</p>
    <h3>Adding</h3>
    <p> The first part is adding. User can add items with following fields. All fields must be filled, not
        empty.<br><br>
        first name _____<br>
        last name _____<br>
        keywords _____ (words describing the person, e.g. html, ysu, 19, etc.)<br>
        resume attach button (file can be upload, extension of uploaded file must be limited to .doc(x), .pdf,
        .txt)<br><br></p>
    <h3>Searching</h3>
    <p>The data entered is sent to MySQL database and then can be searched from there via these fields:<br><br>
        first name _____<br>
        last name _____<br>
        keywords _____<br><br>
        At least one of the fields above must be used, filling all is not necessary. Item is chosen from
        Database and shown in case it or part of it contains searched terms. If more then one field is filled
        during search, only results with both or three field concurrence are shown.
        The search results are shown like this:
    </p>
    <table>
        <tr>
            <th>first name</th>
            <th>last name</th>
            <th>keywords</th>
            <th>resume</th>
        </tr>
        <tr>
            <th>first name of relevant
                item is shown</th>
            <th>last name of relevant
                item is shown</th>
            <th>keywords of relevant
                item are shown</th>
            <th>link (clicking on the
                link opens ''save as''
                window. this part lets
                us to download resume
                we have attached )</th>
        </tr>
    </table>
    <h2>For example.</h2>
    <p>
    I have added the following item besides others:<br>
    first name _armen<br>
    last name _poghosyan<br>
    keywords _ gitc, , phpbeginner, football<br>
    resume – “resume_armen_poghosyan.pdf” file is attached<br>

    After a while, I want to find the item I mentioned, but I remember only part of the information I
    added earlier. So I add in the fields the information I remember and search for it. For example:<br>
    first name _armen<br>
    last name _____ (left empty, i do not remember )<br>
    keywords _php<br><br>
    So the shown result will include discussed item, because “armen” is contained in the first name
    wholly and keyword “php” is included partly (the item had keyword “phpbeginner” which includes
    “php”).<br>
        Other items including searching terms also will be shown.</p>
    <table>
        <tr>
            <th>first name</th>
            <th>last name</th>
            <th>keywords</th>
            <th>resume</th>
        </tr>
        <tr>
            <th>armen</th>
            <th>poghosyan</th>
            <th>gitc, xhtml,
                phpbeginner, football</th>
            <th>link ( downloads “
                resume_armen_poghos
                yan.pdf” )</th>
        </tr>
        <tr>
            <th>armen</th>
            <th>arshakyan</th>
            <th>ysu, css, phpsenior</th>
            <th>link ( downloads “
                resume_armen_arshaky
                an.doc” )</th>
        </tr>
        <tr>
            <th>armenuhi</th>
            <th>sarksyan</th>
            <th>seua, phpmiddle,</th>
            <th>link ( downloads “
                resume_armenuhi_sark
                syan.txt” )</th>
        </tr>
    </table>
</div>
